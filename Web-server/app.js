require('dotenv').config()
const express = require('express')
const hbs = require('hbs');

const app = express()
const port = process.env.PORT;

// Handlebars
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views/partials');

// Servir copntenido estatico
app.use( express.static('public') )

app.get('/', (req, res) => {
  res.render('home', {
    nombre: 'Nicolás',
    titulo: 'Curso de NodeJs'
  });
})

app.get('/generic', (req, res) => {
  res.render('generic', {

  })
})

app.get('/elements', (req, res) => {
  res.render('elements', {

  })
})

// Enviar un archivo html como respuesta
/* app.get('*', (req, res) => {
  res.sendFile(}) */
 
app.listen(port, () => {
    console.log(`Se esta corriendo en http://localhost:${port}`)
  })