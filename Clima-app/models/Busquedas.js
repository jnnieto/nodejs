const fs = require('fs')

const axios = require('axios')

class Busquedas {

    historial = []

    dbPath = './db/database.json'

    constructor() {
        this.leerDB();
    }

    get historialCapitalizado() {
        return this.historial.map( history => {

            let palabras = history.split(' ');
            palabras = palabras.map( p => p[0].toUpperCase() + p.substring(1))

            return palabras.join(' ');

        })
    }

    get paramsMapBox() {
        return{
            'access_token': process.env.MAPBOX_KEY || '',
            'limit': 5,
            'language': 'es'
        }
    }

    async cuidad( termino = '' ) {

        try {
            // Petición http
    
            const instance = axios.create({
                baseURL: `https://api.mapbox.com/geocoding/v5/mapbox.places/${ termino }.json`,
                params: this.paramsMapBox
            })
    
            const resp = await instance.get();

            return resp.data.features.map( lugar => ({
                id: lugar.id,
                nombre: lugar.place_name,
                lng: lugar.center[0],
                lat: lugar.center[1],
            }));            

        } catch (error) {
            return [];
        }

    }

    async climaLugar( lat, lng ) {

        try {
            
            const params = {
                'lat': lat,
                'lon': lng,
                'appid': process.env.WEATHER_KEY || '',
                'units': 'metric',
                'lang': 'es'
            }

            const instance = axios.create({
                baseURL: `https://api.openweathermap.org/data/2.5/weather?`,
                params
            });
            
            const resp = await instance.get();
            const { weather, main } = resp.data;

            return {
                desc: weather[0].description,
                min: main.temp_min,
                max: main.temp_max,
                temp: main.temp
            }

        } catch (error) {
            console.error(error);
        }

    }

    agregarHistorial( lugar = '' ) {

        if ( this.historial.includes( lugar.toLocaleLowerCase() )) {
            return;
        }

        this.historial.unshift( lugar.toLocaleLowerCase() );

        // Grabar en DB
        this.grabarDB();
    }

    grabarDB() {

        const payload = {
            historial: this.historial
        }

        fs.writeFileSync( this.dbPath, JSON.stringify( payload ));
    }

    leerDB() {

        if (!fs.existsSync( this.dbPath )) {
            return;
        }

        const info = fs.readFileSync(this.dbPath, { encoding: 'utf-8' });
        const data = JSON.parse(info);

        this.historial = data.historial;

    }
}

module.exports = Busquedas;