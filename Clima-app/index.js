const { leerInput, inquirerMenu, pausa, listarLugares } = require("./helpers/inquirer");
const Busquedas = require("./models/Busquedas");
require('dotenv').config()


const main = async() => {

    const busquedas = new Busquedas();

    let option;
    
    do {
        option = await inquirerMenu();

        switch( option ) {
            case 1:
                // Mostrar mensaje

                const termino = await leerInput('Cuidad: ');

                // Buscar los lugares

                const lugares = await busquedas.cuidad( termino );
                
                // Selecionar el lugar
                
                const id = await listarLugares(lugares);

                if ( id === '0' ) continue;

                
                const lugarSeleccionado = lugares.find( l => l.id ===  id )
                
                // Guardar en la DB
                busquedas.agregarHistorial( lugarSeleccionado.nombre )

                // Clima

                const clima = await busquedas.climaLugar(lugarSeleccionado.lat, lugarSeleccionado.lng);

                // Mostrar resultados

                console.log('\nInformación de la cuidad\n'.green);                
                console.log('Cuidad:'.cyan, lugarSeleccionado.nombre);
                console.log('Latitud:'.cyan, lugarSeleccionado.lat);
                console.log('Longitud:'.cyan, lugarSeleccionado.lng);
                console.log('Temperatura:'.cyan, clima.temp + " °C");
                console.log('Mínima:'.cyan, clima.min  + " °C");
                console.log('Máxima:'.cyan, clima.max  + " °C");
                console.log('Descripción:'.cyan, clima.desc);
                
            break;
            case 2:

                busquedas.historialCapitalizado.forEach( (history, i) => {
                    const idx = `${ i + 1 }`.green;
                    console.log( `${ idx } ${ history }` );
                })

            break;
            case 0:
            break;
        }

        if ( option !== 0 ) await pausa();

    } while ( option !== 0 );

} 

main();