const inquirer = require('inquirer');
require('colors');

const questions = [
    {
        type: 'list',
        name: 'opcion',
        message: '¿Qué desea hacer?',
        choices: [
            {
                value: '1',
                name: `${ '1.'.blue } Crear tarea`
            },
            {
                value: '2',
                name: `${ '2.'.blue } Listar tareas`
            },
            {
                value: '3',
                name: `${ '3.'.blue } Listar tareas completadas`
            },
            {
                value: '4',
                name: `${ '4.'.blue } Listar tareas pendientes`
            },
            {
                value: '5',
                name: `${ '5.'.blue } Completar tarea(s)`
            },
            {
                value: '6',
                name: `${ '6.'.blue } Borrar tarea`
            },
            {
                value: '0',
                name: `${ '0.'.blue } Salir`
            },
        ]  
    }
]

const inquirerMenu = async() => {

    console.clear();
    console.log('================================='.green);
    console.log('   Seleccione una opcion   '.red);
    console.log('================================='.green);

    const { opcion } = await inquirer
        .prompt(questions);

    return opcion;

}

const pausa = async() => {
    const response = await inquirer
        .prompt([
            {
                type: 'input',
                name: 'respuesta',
                message: `Presiona ${ 'ENTER'.green } para continuar...`
            }
        ])

        console.log('\n');
    return response;
}

const leerInput = async( message ) => {
    const question = [
        {
            type: 'input',
            name: 'desc',
            message,
            validate( value ) {
                if ( value.length === 0 ) {
                    return 'Por favor ingrese un valor';
                }
                return true;
            } 
        }
    ];


    const { desc } = await inquirer
        .prompt(question)

    return desc;
}

const listadoTareasBorrar = async( tareas = [] ) => {

    const choices = tareas.map( (tarea, i) => {

        const idx = `${ (i + 1) + '.' }`.green;
        
        return {
            value: tarea.id,
            name: `${ idx } ${ tarea.descripcion }`,
        }
    })

    choices.unshift({
        value: '0',
        name: '0.'.green + ' Cancelar'
    })

    const question = [
        {
            type: 'list',
            name: 'id',
            message: '¿Qué tarea desea eliminar?',
            choices
        }
    ]

    const { id } = await inquirer
        .prompt(question)

    return id;
}

const confirmar = async( message ) => {

    const question = [
        {
            type: 'confirm',
            name: 'ok',
            message
        }
    ]

    const { ok } = await inquirer.prompt(question);

    return ok;

}

const mostrarlistadoChecklist = async( tareas = [] ) => {

    const choices = tareas.map( (tarea, i) => {

        const idx = `${ (i + 1) + '.' }`.green;
        
        return {
            value: tarea.id,
            name: `${ idx } ${ tarea.descripcion }`,
            checked: ( tarea.completadoEn ) ? true : false
        }
    })

    const question = [
        {
            type: 'checkbox',
            name: 'ids',
            message: 'Selecciones',
            choices
        }
    ]

    const { ids } = await inquirer
        .prompt(question)

    return ids;
}

module.exports = {
    inquirerMenu,
    pausa,
    leerInput,
    listadoTareasBorrar,
    confirmar,
    mostrarlistadoChecklist
}