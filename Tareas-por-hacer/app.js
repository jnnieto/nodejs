require('colors')
const { guardarDB, leerDB } = require('./helpers/guardarArchivo');
const { inquirerMenu, pausa, leerInput, listadoTareasBorrar, confirmar, mostrarlistadoChecklist } = require('./helpers/inquirer');
const Tareas = require('./models/Tareas');

// const { mostrarMenu, pausa } = require('./helpers/mensajes');

console.clear();

const main = async() => {
    let option = '';
    const tareas = new Tareas();

    const tareasDB = leerDB()

    if (tareasDB) {
        tareas.cargarTareasFromArray(tareasDB)
    }

    do {

        option = await inquirerMenu()

        switch(option) {
            case '1':
                const desc = await leerInput('Descripcion: ');
                console.log(desc);
                tareas.crearTarea(desc);
                break;
            case '2':
                tareas.listadoCompleto();   
                // console.log( tareas.listadoArr );
                break;
            case '3':
                tareas.listarCompletadasPendientes(true);
                break;
            case '4':
                tareas.listarCompletadasPendientes(false);
                break;
            case '5':
                const ids = await mostrarlistadoChecklist(tareas.listadoArr)
                tareas.toggleCompletadas( ids )
                break;
            case '6':
                const id = await listadoTareasBorrar(tareas.listadoArr);

                if ( id !== '0') {
                    
                    const isdelete = await confirmar('¿Estás seguro de borrar la tarea?')
    
                    if ( isdelete ) {
                        tareas.borrarTarea( id );
                        console.log('Tarea borrada satisfactoriamente');
                    }

                }


                break;
            case '0':
                break;
        }

        guardarDB(tareas.listadoArr);

        await pausa();
        
    } while ( option !== '0' );
    //  pausa();
}

main();