const Tarea = require("./Tarea");
require('colors');
/**
 * _listado:
 *  {}
 */
class Tareas {
    _listado = {}

    get listadoArr() {
        const listado = [];

        Object.keys(this._listado).forEach( key => {
            listado.push(this._listado[key])
        })

        return listado;
    }

    constructor() {
        this._listado = {};
    }
    
    cargarTareasFromArray(tareas = []) {

        tareas.forEach( tarea => {
            this._listado[tarea.id] = tarea;
        } )

    }

    crearTarea(desc = '') {
        const tarea = new Tarea(desc);
        this._listado[tarea.id] = tarea;
    }

    listadoCompleto() {

        let index = 1;
        for (let tarea of this.listadoArr) {

            if (tarea.completadoEn === null) {
                console.log(`${ index }. ${ tarea.descripcion} :: ${ 'Pendiente'.red } `);
            } else {
                console.log(`${ index }. ${ tarea.descripcion } :: ${ 'Completada'.green } `);
            }
            index++;
        }
        console.log('\n');
    }

    listarCompletadasPendientes( completada = true ) {
        let index = 0;
        console.log('\n');
        for (let tarea of this.listadoArr) {
            const { descripcion, completadoEn } = tarea;
            const estado = (completadoEn)
                                ? `Completada en ${ completadoEn }`.green
                                : 'Pendiente'.red
            
            if ( completada ) {
                if ( completadoEn ) {
                    index += 1;
                    console.log(`${ (index + '.').green  } ${ descripcion } :: ${ estado } `);
                }
            } else {
                if ( !completadoEn ) {
                    index += 1;
                    console.log(`${ (index + '.').green  } ${ descripcion } :: ${ estado } `);
                }
            }
        }
        console.log('\n');
    }

    listarTareasPendientes() {
        let index = 1;
        for (let tarea of this.listadoArr) {

            if (tarea.completadoEn === null) {
                console.log(`${ index }. ${ descripcion } :: ${ 'Pendiente'.red } `);
            }
            index++;
        }
        console.log('\n');
    }

    borrarTarea( id = '' ) {

        if (this._listado[id]) {
            delete this._listado[id]
        }
    }

    toggleCompletadas( ids = []) {
        ids.forEach( id => {
            const tarea = this._listado[id];

            if( !tarea.completadoEn ) {
                tarea.completadoEn = new Date().toISOString();
            }
        });

        this.listadoArr.forEach( tarea => {
            if (!ids.includes(tarea.id)) {
                this._listado[tarea.id].completadoEn = null;
                
            }
        });
    }
}

module.exports = Tareas;