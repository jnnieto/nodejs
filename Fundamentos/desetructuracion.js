const deadpool = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneracion',
    getNombre(hola) {
        return `${ hola } ${ this.nombre } ${ this.apellido }`;
    }
}

console.log(deadpool.getNombre('Mi nombre es')); 

// const nombre = deadpool.nombre
// const apellido = deadpool.apellido
// const poder = deadpool.poder

function imprimeHeroe({ nombre, apellido, poder}) {
    nombre = 'Nicolas';
    console.log(nombre, apellido, poder);

}

imprimeHeroe(deadpool);

const heroes = ['Deadpool', 'Superman', 'Batman'];

const [ , h2, ] = heroes

for (let hero of heroes) {
    console.log(hero);
}

