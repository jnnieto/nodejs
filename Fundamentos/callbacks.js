/* setTimeout(() => {
   console.log('Hola mundo'); 
}, 2000); */


const getUsuarioByID = (id, callback) => {
    const usuario = {
        id,
        nombre: 'Nicolas'
    }

    setTimeout(() => {
       callback(usuario); 
    }, 1500);
}


getUsuarioByID(10, (usuario) => {
    console.log(usuario.id, usuario.nombre.toUpperCase());
});