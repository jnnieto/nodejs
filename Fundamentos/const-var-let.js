var nombre = 'Wolverine';
// Problemas de var es que crea una variable de ambito global

// Let es de ambito de bloque 
if (true) {
    var nombre = 'Magneto'
    console.log(nombre);
}
var nombre;

console.log(nombre);