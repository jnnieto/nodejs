const nombre = 'Deadpool';

const real = 'Wade Winston';

const normal = nombre + ' ' + real;

const template = `${nombre} Nicolas Nieto`;

// Los templates permiten trabajar con multilineado

const html = `
<h1>Hola</h1>
<p>Mundo</p>
`;

console.log(normal);
console.log(template);

console.log(normal === template);