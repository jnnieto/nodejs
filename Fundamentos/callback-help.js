const empleados = [
    {
        id: 1,
        nombre: 'Nicolas'
    },
    {
        id: 2,
        nombre: 'Johann'
    },
    {
        id: 3,
        nombre: 'Nieto'
    }
]


const salarios = [
    {
        id: 1,
        salario: 1000
    },
    {
        id: 2,
        salario: 1500
    }
]

const getEmpleado = (id, callback) => {
    const empleado = empleados.find( (e) => e.id === id)

    if (empleado) {
        callback(null, empleado);
    } else {
        callback(null, `Empleado con id ${ id } no existe`)
    }

    return empleado
}

const getSalario = (id, callback) => {
    const salario = salarios.find(e => e.id === id)?.salario

    const result = (salario) ? callback(null, salario) : callback(null, `El salario con id ${ id } no existe`)

    return result;
}

getEmpleado(2, (error, empleado) => {

    if (error) {
        console.log('ERROR');
        return console.log(error);
    }

    getSalario(2, (err, salario) => {

        if (err) {
            console.log('OCURRIO UN ERROR', err);
        }
        console.log('Salario: ', salario);
    
    })
})

