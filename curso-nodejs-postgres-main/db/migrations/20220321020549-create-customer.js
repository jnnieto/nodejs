'use strict';

const { CUSTOMERS_TABLE, CustomerSchema } = require("../models/customer.model");

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable(CUSTOMERS_TABLE, CustomerSchema);
  },

  down: async (queryInterface) => {
    await queryInterface.dropTable(CUSTOMERS_TABLE);
  }
};
