'use strict';

const { CATEGORY_TABLE, CategorySchema } = require('../models/category.model');


module.exports = {
  async up (queryInterface) {
    await queryInterface.changeColumn(CATEGORY_TABLE, 'name', CategorySchema.name);
  },

  async down (queryInterface) {

  }
};
