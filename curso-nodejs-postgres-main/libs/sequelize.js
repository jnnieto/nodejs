const { Sequelize } = require('sequelize');
const { config } = require('../config/config');

const setupModels = require('../db/models/index');

const USER = encodeURIComponent(config.dbUser);
const PASSWORD = encodeURIComponent(config.dbPassword);
const URI = `postgres://${ USER }:${ PASSWORD }@${config.dbHost}:${config.dbPort}/${config.dbName}`;

const sequelize = new Sequelize(URI, {
  dialect: 'postgres',
  logging: false,
});

setupModels(sequelize);

// Se quita porque todo se hará por medio de migraciones
/* sequelize.sync() */

sequelize.authenticate()
    .then(() => console.log("Connection has been established successfully."))
    .catch(err => console.error("Unable to connect to the database: ", err));

module.exports = sequelize;
